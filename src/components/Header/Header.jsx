import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


Header.propTypes = {}

function Header(props) {

    return (
        <nav className="h-14 bg-blue-100 text-center font-serif">
            <ul className="flex h-14 mx-36">
              <li className="w-1/4 text-2xl  h-14 hover:bg-blue-200"><Link to="/count">Count</Link></li>
              <li className="w-1/4 text-2xl  h-14 hover:bg-blue-200"><Link to="/calculator">Calculator</Link></li>
              <li className="w-1/4 text-2xl  h-14 hover:bg-blue-200"><Link to="/todo">Todo</Link></li>
              <li className="w-1/4 text-2xl  h-14 hover:bg-blue-200"><Link to="/quote">Quote</Link></li>
            </ul>
          </nav>
    )
}

export default Header;